<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
        <title>{{project_name}}</title>
        <meta content="Admin Dashboard" name="description">
        <meta content="Xplex" name="author">
        <link rel="shortcut icon" href="{{url('public/dashboard/assets/images/favicon for dasboard.png')}}">
        <!--calendar css-->
        <link href="{{url('dashboard/plugins/fullcalendar/css/fullcalendar.min.css')}}" rel="stylesheet">
        <link href="{{url('dashboard/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{url('dashboard/assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{url('dashboard/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{url('dashboard/assets/css/style.css')}}" rel="stylesheet" type="text/css">
    </head>

    <body>
    <!-- Begin page -->
    <div id="wrapper">
        <div class="container">
            <div class="home-btn d-none d-sm-block"><a href="{{url('/')}}" class="text-dark"><i
                            class="fas fa-home h2"></i></a></div>
            <div class="wrapper-page">
                <div class="card overflow-hidden account-card mx-3">
                    <div class="bg-primary p-4 text-white text-center position-relative">
                        <h4 class="font-20 m-b-5">Welcome Back !</h4>
                        <p class="text-white-50 mb-4">Sign in to continue to {{project_name}} - Dashboard.</p><a
                                href="index.html"
                                class="logo logo-admin"><img
                                    src="{{url('dashboard/assets/images/logo-sm for dashboard.png')}}" height="24"
                                    alt="logo" style="height: 50px"></a>
                    </div>
                    <div class="account-card-content">
                        <form class="form-horizontal m-t-30" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="username">email</label> <input type="text" name="email" class="form-control"
                                                                           id="email" placeholder="Enter email">

                                @if ($errors->has('email'))
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>

                                    </span>
                                @endif
                            </div>
                            <div class="form-group"><label for="userpassword">Password</label>
                                <input type="password" class="form-control" id="userpassword" name="password"
                                       placeholder="Enter password">
                                @if ($errors->has('password'))
                                    <span class="text-danger " role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group row m-t-20">
                                <div class="col-sm-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customControlInline">
                                        <input class="custom-control-input" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-control-label" for="customControlInline">Remember
                                            me</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In
                                    </button>
                                </div>
                            </div>
                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        <i class="mdi mdi-lock"></i> {{ __('Forgot Your Password?') }}
                                    </a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="m-t-40 text-center">

                    <p>© 2019 Turtle Ventures - Dashboard. Crafted with <i class="mdi mdi-heart text-danger"></i> by
                        Turtle
                        Ventures</p>
                </div>
            </div><!-- end wrapper-page -->
        </div>
    </div>
    <script src="{{url('dashboard/assets/js/jquery.min.js')}}"></script>
    <script src="{{url('dashboard/assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{url('dashboard/assets/js/metisMenu.min.js')}}"></script>
    <script src="{{url('dashboard/assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{url('dashboard/assets/js/waves.min.js')}}"></script><!-- Jquery-Ui -->
    <script src="{{url('dashboard/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{url('dashboard/plugins/moment/moment.js')}}"></script>
    <script src="{{url('dashboard/plugins/fullcalendar/js/fullcalendar.min.js')}}"></script>
    <script src="{{url('dashboard/assets/pages/calendar-init.js')}}"></script><!-- App js -->
    <script src="{{url('dashboard/assets/js/app.js')}}"></script>
    </body>
</html>
