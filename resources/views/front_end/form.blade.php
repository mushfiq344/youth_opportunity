<!doctype html>
<html lang="en">

    <head>
        @include('front_end.partials.head')
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>

    </head>

    <body style="background-color: #fbfbfd">

        <div class="body_wrapper">
            @include('front_end.partials.header')
            <section class="breadcrumb_area"
                     style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('{{url("dashboard/images/default_images/post_opp_cover.jpg")}}'); background-position:  center center; background-repeat: no-repeat; background-size: cover;">
                <img class="breadcrumb_shap" src="" alt="">
                <div class="container">
                    <div class="breadcrumb_content text-center">
                        <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Post an Opportunity</h1>

                    </div>
                </div>
            </section>
            <section class="job_details_area sec_pad">
                <div class="container custom_container">
                    <div class="row flex-row-reverse">
                        <div class="col-lg-4 pl_30">
                            <div class="job_info">
                                <div>
                                    <div class="mt_30">
                                        <div class="widget_title">
                                            <h3 class="f_p f_size_20 t_color3"><i class="fas fa-bolt"></i> Trending</h3>
                                            <div class="border_bottom"></div>
                                        </div>
                                        @foreach($trendings as $trending)
                                            <div>
                                                <div class="media post_item">
                                                    <img height="70px" width="70px" src="{{$trending->image}}" alt="">
                                                    <div class="blog-sidebar media-body">
                                                        <a href="{{route('get_opp',['id'=>$trending->id])}}">
                                                            <h3 class="f_size_16 f_p f_400">{{$trending->title}}.
                                                            </h3>
                                                        </a>
                                                        <div class="entry_post_info">
                                                            By: <a href="{{route('get_opp',['id'=>$trending->id])}}}}">Admin</a>
                                                            <a href="{{route('get_opp',['id'=>$trending->id])}}">{{date('F j, Y', strtotime($trending->created_at))}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        @endforeach
                                    </div>
                                    <div class="f_social_icon_two text-center">
                                        <a href="#"><i class="ti-facebook"></i></a>
                                        <a href="#"><i class="ti-twitter-alt"></i></a>
                                        <a href="#"><i class="ti-vimeo-alt"></i></a>
                                        <a href="#"><i class="ti-pinterest"></i></a>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="details_content">
                                {{--<div class="sec_title mb_70">--}}

                                {{--<h3 class="f_p f_size_22 f_500 t_color3 mt_30 mb_20">Job Requirements</h3>--}}
                                {{--<p class="f_400 f_size_15">Why I say old chap that is spiffing pukka, bamboozled--}}
                                {{--wind up--}}
                                {{--bugger buggered zonked hanky panky a blinding shot the little rotter, bubble and--}}
                                {{--squeak--}}
                                {{--vagabond cheeky bugger at public school pardon you bloke the BBC. Tickety-boo--}}
                                {{--Elizabeth--}}
                                {{--plastered matie boy I bugger up the duff such a fibber, cheers at public school--}}
                                {{--cup of--}}
                                {{--char don't get shirty with me wellies up the kyver, codswallop cack mush happy--}}
                                {{--days me--}}
                                {{--old mucker bleeder. Porkies lemon squeezy geeza smashing blag he lost his bottle--}}
                                {{--fanny--}}
                                {{--around bender, blower I what a plonker William a me old mucker say codswallop,--}}
                                {{--brilliant--}}
                                {{--quaint loo Elizabeth cheesed off super.!</p>--}}
                                {{--</div>--}}
                                {{--<iframe width="100%" height="600" src="https://www.youtube.com/embed/FWkTH9MuEnI"--}}
                                {{--frameborder="0"--}}
                                {{--allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"--}}
                                {{--allowfullscreen></iframe>--}}
                                <div class="sec_title mb_70 mt_30">

                                    <form action="{{route('insert_form',['type'=>'opportunity'])}}"
                                          class="row apply_form" enctype="multipart/form-data" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Title <span style="color: red">*</span></label>
                                            <input name="opportunity_title" type="text"
                                                   placeholder="Enter Your Project Title" required>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="">Opportunity Type </label>
                                            <select class="form-control" name="opportunity_type" required>
                                                <option value="">Choose...</option>
                                                @foreach($opp_types as $opportunity_type)
                                                    <option value="{{$opportunity_type->id}}">{{$opportunity_type->type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Opportunity Description <span
                                                        style="color: red">*</span></label>
                                            <textarea class="summernote" name="detail" cols="30" rows="10"
                                                      placeholder="Summernote Needed Here" required></textarea>
                                        </div>

                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Application Process <span
                                                        style="color: red">*</span></label><br>
                                            Please write on how to apply for this opportunity.
                                            <textarea class="summernote" name="application_process" cols="30" rows="10"
                                                      placeholder="Type Application Process Here" required></textarea>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Benefits <span style="color: red">*</span></label><br>
                                            Please write what the participants will gain from this opportunity.
                                            <textarea class="summernote" name="benefits" id="message" cols="30"
                                                      rows="10"
                                                      placeholder="Type Benefits Here" required></textarea>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Eligibilities <span
                                                        style="color: red">*</span></label><br>
                                            Please write what the participants Eligibilities.
                                            <textarea class="summernote" name="eligibilities" id="message" cols="30"
                                                      rows="10"
                                                      placeholder="Type Benefits Here" required></textarea>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Region <span
                                                        style="color: red">*</span></label>
                                            <select class="form-control" name="region" required>
                                                <option value="">Choose...</option>
                                                @foreach($regions as $region)
                                                    <option value="{{$region->id}}">{{$region->type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Funding Type <span
                                                        style="color: red">*</span></label>
                                            <select class="form-control" name="funding_type" required>
                                                <option value="">Choose...</option>
                                                @foreach($funding_types as $fund)
                                                    <option value="{{$fund->id}}">{{$fund->type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Degree Type <span
                                                        style="color: red">*</span></label>
                                            <select class="form-control" name="degree_type" required>
                                                <option value="">Choose...</option>
                                                @foreach($degree_types as $degree)
                                                    <option value="{{$degree->id}}">{{$degree->type}}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Address <span
                                                        style="color: red">*</span></label>
                                            <input name="address" type="text" placeholder="Type Address" required>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Apply Link <span style="color: red">*</span></label>
                                            <input type="text" placeholder="Paste Link Here" name="link" required>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Official Link <span style="color: red">*</span></label>
                                            <input type="text" placeholder="Paste Link Here" name="website_link"
                                                   required>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Start Date <span
                                                        style="color: red">*</span></label>
                                            <input class="form-control" name="start_date" type="date"
                                                   value="{{date('Y-m-d')}}" placeholder="Type Start Date" required>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">End Date <span
                                                        style="color: red">*</span></label>
                                            <input class="form-control" name="end_date" type="date"
                                                   value="{{date('Y-m-d')}}" placeholder="Type Start Date" required>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Deadline <span
                                                        style="color: red">*</span></label>
                                            <input class="form-control" name="deadline" type="date"
                                                   value="{{date('Y-m-d')}}" placeholder="Type Start Date" required>
                                        </div>
                                        @include('admin_panel.forms.upload.partials.country_state_city')
                                        <div class="form-group col-lg-12">
                                            <label class="inpl">Contact info<span
                                                        style="color: red">*</span></label>
                                            <input name="contact_info" type="text"
                                                   placeholder="Type Your Contact Info Here" required>
                                        </div>

                                        <div class="form-group col-lg-12">
                                            <div class="upload_box"> Featured Image <span style="color: red">*</span>:
                                                <input type="file" accept="image/x-png,image/gif,image/jpeg"
                                                       name="cover" id="File" required>
                                            </div>
                                        </div>


                                        <div class="col-lg-12">
                                            <p>By clicking apply job you agree to our terms that you have read in
                                                our<br> <a
                                                        href="#">Terms &amp; Conditions</a> page.</p>
                                            <button type="submit" class="btn_three">Apply Now</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('front_end.partials.footer_codes')
        </div>
        <!-- Optional JavaScript -->
        <script>
            $('.summernote').summernote({
                tabsize: 2,
                height: 100
            });
        </script>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        @include('front_end.partials.footer_links_for_index')
    </body>

</html>