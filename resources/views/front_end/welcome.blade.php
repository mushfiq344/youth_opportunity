@extends('front_end.layouts.main')
@section('content')
    <section class="saas_home_area" style="background-image:  linear-gradient(to bottom, rgba(0, 0, 0, 0.30), rgba(0, 0, 0, 0.30)), url('{{url('dashboard/images/default_images/home_cover.jpg')}}')">
        <div class="banner_top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="f_p f_size_40 l_height60 wow fadeInUp" data-wow-delay="0.3s"> <span
                                    class="f_700">Find Your Scholarsips!</span>
                        </h2>
                        <p class="f_size_18 l_height30 wow fadeInUp" data-wow-delay="0.5s">Find the Best Scholarships Around the Globe</p>
                        <form method="post" action="{{route('post_explore_opp')}}">
                            {{ csrf_field() }}
                            <div class="input-group subcribes"
                                 style="border: snow 1px solid; padding: 10px; border-radius: 10px; background-color: #FFFFFF">
                                <select class="selectpickers" style="display: none;" name="type">
                                    @foreach($opp_types as $type)
                                        <option value="{{$type->id}}">{{$type->type}}</option>
                                    @endforeach
                                </select> <span style="color: #00BB69; padding: 7px;">in</span>
                                <select class="selectpickers" style="display: none;" name="region">
                                    @foreach($regions as $region)
                                        <option value="{{$region->id}}">{{$region->type}}</option>
                                    @endforeach
                                </select>
                                <span style="color: #00BB69; padding: 7px;">With</span>
                                <select class="selectpickers" style="display: none;" name="funding_type">
                                    @foreach($funding_types as $fund)
                                        <option value="{{$fund->id}}">{{$fund->type}}</option>
                                    @endforeach

                                </select>
                                <button type="submit" class="btn btn_submit f_size_15 f_500">Find Scholarship</button>
                            </div>
                            <p class="mchimp-errmessage" style="display: none;"></p>
                            <p class="mchimp-sucmessage" style="display: none;"></p>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="service_promo_area">
        <div class="shape_top">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="shape shape_one">
                <defs>
                    <linearGradient>
                        <stop offset="0%" stop-color="rgb(76,1,124)" stop-opacity="0.95"/>
                        <stop offset="100%" stop-color="rgb(103,84,226)" stop-opacity="0.95"/>
                    </linearGradient>
                </defs>
                <path d="M121.891,264.576 L818.042,11.198 C914.160,-23.786 1020.439,25.773 1055.424,121.890 L1308.802,818.041 C1343.786,914.159 1294.227,1020.439 1198.109,1055.422 L501.958,1308.801 C405.840,1343.785 299.560,1294.226 264.576,1198.108 L11.198,501.957 C-23.786,405.839 25.773,299.560 121.891,264.576 Z"/>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="shape shape_two">
                <defs>
                    <linearGradient id="PSgrad_0">
                        <stop offset="0%" stop-color="rgb(76,1,124)" stop-opacity="0.95"/>
                        <stop offset="100%" stop-color="rgb(103,84,226)" stop-opacity="0.95"/>
                    </linearGradient>

                </defs>
                <path fill="url(#PSgrad_0)"
                      d="M121.891,264.575 L818.042,11.198 C914.160,-23.786 1020.439,25.772 1055.424,121.890 L1308.802,818.040 C1343.786,914.159 1294.227,1020.439 1198.109,1055.423 L501.958,1308.801 C405.840,1343.785 299.560,1294.226 264.576,1198.107 L11.198,501.957 C-23.786,405.839 25.773,299.560 121.891,264.575 Z"/>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                 class="shape shape_three">
                <defs>
                    <linearGradient id="PSgrad_1">
                        <stop offset="0%" stop-color="rgb(76,1,124)" stop-opacity="0.95"/>
                        <stop offset="100%" stop-color="rgb(103,84,226)" stop-opacity="0.95"/>
                    </linearGradient>

                </defs>
                <path fill="url(#PSgrad_1)"
                      d="M1198.109,264.576 L501.958,11.198 C405.840,-23.787 299.560,25.772 264.576,121.891 L11.198,818.041 C-23.786,914.159 25.773,1020.439 121.891,1055.422 L818.042,1308.801 C914.160,1343.785 1020.439,1294.226 1055.424,1198.108 L1308.802,501.957 C1343.786,405.839 1294.227,299.560 1198.109,264.576 Z"/>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="shape shape_four">
                <defs>
                    <linearGradient id="PSgrad_2">
                        <stop offset="0%" stop-color="rgb(76,1,124)" stop-opacity="0.95"/>
                        <stop offset="100%" stop-color="rgb(103,84,226)" stop-opacity="0.95"/>
                    </linearGradient>

                </defs>
                <path fill="url(#PSgrad_2)"
                      d="M1198.109,264.575 L501.958,11.198 C405.840,-23.787 299.560,25.772 264.576,121.890 L11.198,818.040 C-23.786,914.158 25.773,1020.439 121.891,1055.423 L818.042,1308.801 C914.160,1343.785 1020.440,1294.225 1055.424,1198.107 L1308.802,501.957 C1343.786,405.839 1294.227,299.560 1198.109,264.575 Z"/>
            </svg>
        </div>
        <div class="container-fluid">
            <div class="sec_title mb_70 wow fadeInUp" data-wow-delay="0.4s"
                 style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
            </div>
            <ul class="nav nav-tabs startup_tab">
                <?php  $i=0; ?>
                @foreach($opp_types as $type)

                    <li class="nav-item">
                        <a class="nav-link" href="{{route('get_explore_opp',['type'=>$type->id,'region'=>'all','funding_type'=>'all','deadline'=>'all','degree_type'=>'all'])}}">
                            <span class="icon" style="background:{{color_codes[fmod($i, 5)]}}">
                                <i class="fas {{$type->code}}"></i>
                            </span>
                            <h3>{{$type->type}}</h3>
                        </a>
                    </li>
                    <?php  $i++; ?>
                @endforeach
            </ul>
        </div>

        <div class="container custom_container">
           <div class="owl-carousel owl-theme">
                @foreach($objects as $object)
                            <div class="cloned" style="width: 100%; margin-right: 15px;">
                                <div class="h_blog_item pos_blog_item">
                                    <img src="{{$object->image}}" alt="">
                                    <div class="h_blog_content">
                                        <a href="{{route('get_opp',['id'=>$object->id])}}" class="post_time"><i
                                                    class="icon_clock_alt"></i>March 20, 2019</a>
                                        <a href="{{route('get_opp',['id'=>$object->id])}}"><h3>{{$object->title}}..</h3>
                                        </a>
                                        <div class="post-info-bottom">
                                            <a href="{{route('get_opp',['id'=>$object->id])}}" class="learn_btn_two">Read
                                                More <i class="arrow_right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        
</div>

        </div>

    </section>
    <section class="software_promo_area sec_pad">
        <div class="container  custom_container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="widget_title">
                        <h3 class="f_p f_size_20 t_color3"><i class="fas fa-hourglass-start"></i> Deadline Approaching
                        </h3>
                        <div class="border_bottom"></div>
                    </div>
                    <div class="row">
                        @foreach($upcoming_events as $event)
                            <div class="col-lg-6 mb-4">
                                <div class="media post_item">
                                    <img height="70px" width="70px" src="{{$event->image}}" alt="">
                                    <div class="blog-sidebar media-body">
                                        <a href="#">
                                            <h3 class="f_size_16 f_p f_400">{{$event->title}}.</h3>
                                        </a>
                                        <div class="entry_post_info">
                                            By: <a href="{{route('get_opp',['id'=>$event->id])}}">Admin</a>
                                            <a href="{{route('get_opp',['id'=>$event->id])}}">{{date('F j, Y', strtotime($event->created_at))}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <br>
                </div>
                <div class="col-lg-4">
                    <div class="widget_title">
                        <h3 class="f_p f_size_20 t_color3"><i class="fas fa-bolt"></i> Trending</h3>
                        <div class="border_bottom"></div>
                    </div>
                    @foreach($trendings as $trending)
                        <div>
                            <div class="media post_item">
                                <img height="70px" width="70px" src="{{$trending->image}}" alt="">
                                <div class="blog-sidebar media-body">
                                    <a href="{{route('get_opp',['id'=>$trending->id])}}">
                                        <h3 class="f_size_16 f_p f_400">{{$trending->title}}.
                                        </h3>
                                    </a>
                                    <div class="entry_post_info">
                                        By: <a href="{{route('get_opp',['id'=>$trending->id])}}}}">Admin</a>
                                        <a href="{{route('get_opp',['id'=>$trending->id])}}">{{date('F j, Y', strtotime($trending->created_at))}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    @endforeach

                </div>
                <div class="col-lg-8">
                    <div class="widget_title">
                        <h3 class="f_p f_size_20 t_color3"><i class="fas fa-hourglass-start"></i> Opportunities in Bangladesh
                        </h3>
                        <div class="border_bottom"></div>
                    </div>
                    <div class="row">
                        @foreach($bangladeshis as $event)
                            <div class="col-lg-6 mb-4">
                                <div class="media post_item">
                                    <img height="70px" width="70px" src="{{$event->image}}" alt="">
                                    <div class="blog-sidebar media-body">
                                        <a href="#">
                                            <h3 class="f_size_16 f_p f_400">{{$event->title}}.</h3>
                                        </a>
                                        <div class="entry_post_info">
                                            By: <a href="{{route('get_opp',['id'=>$event->id])}}">Admin</a>
                                            <a href="{{route('get_opp',['id'=>$event->id])}}">{{date('F j, Y', strtotime($event->created_at))}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <br>
                </div>

            </div>
        </div>
    </section>

    <section class="s_subscribe_area">
        <div class="s_shap">
            <svg class="right_shape" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <defs>
                    <linearGradient id="PSgrad_5">
                        <stop offset="0%" stop-color="rgb(103,84,226)" stop-opacity="0.95"/>
                        <stop offset="100%" stop-color="rgb(25,204,230)" stop-opacity="0.95"/>
                    </linearGradient>
                </defs>
                <path fill="url(#PSgrad_5)"
                      d="M543.941,156.289 L227.889,41.364 C184.251,25.497 136.000,47.975 120.118,91.571 L5.084,407.325 C-10.799,450.921 11.701,499.127 55.339,514.995 L371.391,629.920 C415.029,645.788 463.280,623.309 479.162,579.713 L594.196,263.959 C610.079,220.362 587.579,172.157 543.941,156.289 Z"/>
                <path fill="url(#PSgrad_5)"
                      d="M625.661,120.004 L309.609,5.079 C265.971,-10.790 217.720,11.689 201.838,55.286 L86.804,371.039 C70.921,414.636 93.421,462.842 137.059,478.709 L453.111,593.634 C496.749,609.502 545.000,587.024 560.882,543.427 L675.916,227.673 C691.799,184.077 669.299,135.872 625.661,120.004 Z"/>
            </svg>
            <svg class="bottom_shape" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <defs>
                    <linearGradient id="PSgrad_6" x1="76.604%" x2="0%" y1="0%" y2="64.279%">
                        <stop offset="0%" stop-color="rgb(103,84,226)" stop-opacity="0.95"/>
                        <stop offset="100%" stop-color="rgb(25,204,230)" stop-opacity="0.95"/>
                    </linearGradient>
                </defs>
                <path fill="url(#PSgrad_6)"
                      d="M543.941,156.289 L227.889,41.365 C184.251,25.496 136.000,47.975 120.118,91.572 L5.084,407.325 C-10.799,450.922 11.701,499.127 55.339,514.995 L371.391,629.920 C415.029,645.788 463.280,623.310 479.162,579.713 L594.196,263.959 C610.079,220.362 587.579,172.157 543.941,156.289 Z"/>
                <path fill="url(#PSgrad_6)"
                      d="M625.661,120.004 L309.609,5.078 C265.971,-10.789 217.720,11.689 201.838,55.286 L86.804,371.040 C70.921,414.636 93.421,462.842 137.059,478.709 L453.111,593.634 C496.749,609.502 545.000,587.023 560.882,543.427 L675.916,227.673 C691.799,184.077 669.299,135.871 625.661,120.004 Z"/>
            </svg>
        </div>
        <div class="container">
            <div class="sec_title text-center mb_50 wow fadeInUp" data-wow-delay="0.4s">
                <h2 class="f_p f_size_30 l_height50 f_600 t_color">Scholarship Hits!</h2>
                <p class="f_400 f_size_18 l_height34">Get week's most popular scholarships!</p>
            </div>
            <form action="{{route('insert_form_subs',['type'=>'subscriber'])}}"  method="post">
                {{ csrf_field() }}
                <div class="input-group s_subcribes">
                    <input type="text" name="email" type="email" class="form-control memail" placeholder="E-Mail Address" required>
                    <button class="btn btn-submit"  type="submit"><i class="ti-arrow-right"></i></button>
                </div>
                <p class="mchimp-errmessage" style="display: none;"></p>
                <p class="mchimp-sucmessage" style="display: none;"></p>
            </form>
        </div>
    </section>
@endsection
@section('js_links')
    @include('front_end.partials.footer_links_for_index')
@endsection

