<footer class="footer_area f_bg">
    {{--edit here--}}
    {{--<div class="footer_top">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-3 col-md-6">--}}
                    {{--<div class="f_widget company_widget wow fadeInLeft" data-wow-delay="0.2s">--}}
                        {{--<a href="{{url('/')}}" class="f-logo"><img--}}
                                    {{--src="{{url('front_end/img/logo - footer.png')}}"--}}
                                    {{--srcset="img/logo2x.png 2x"--}}
                                    {{--alt=""></a>--}}
                        {{--<p class="f_400 f_p f_size_15 mb-0 l_height28 mt_30">Why I say old chap that is--}}
                            {{--sping--}}
                            {{--lavatory chip shop gosh off his, smashing boot are you taking the piss posh loo--}}
                            {{--brilliant matie boy young.!!</p>--}}
                        {{--<div class="widget-wrap">--}}
                            {{--<p class="f_400 f_p f_size_15 mb-0 l_height34"><span>Email:</span> <a--}}
                                        {{--href="mailto:saasland@gmail.com"--}}
                                        {{--class="f_400">saasland@gmail.com</a></p>--}}
                            {{--<p class="f_400 f_p f_size_15 mb-0 l_height34"><span>Phone:</span> <a--}}
                                        {{--href="tel:948256347968" class="f_400">+948 256 347 968</a></p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-3 col-md-6">--}}
                    {{--<div class="f_widget about-widget pl_70 wow fadeInLeft" data-wow-delay="0.4s">--}}
                        {{--<h3 class="f-title f_600 t_color f_size_18 mb_40">About Us</h3>--}}
                        {{--<ul class="list-unstyled f_list">--}}
                            {{--<li><a href="#">Company</a></li>--}}
                            {{--<li><a href="#">Leadership</a></li>--}}
                            {{--<li><a href="#">Diversity</a></li>--}}
                            {{--<li><a href="#">Jobs</a></li>--}}
                            {{--<li><a href="#">Press</a></li>--}}
                            {{--<li><a href="#">Wavelength</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-3 col-md-6">--}}
                    {{--<div class="f_widget about-widget pl_20 wow fadeInLeft" data-wow-delay="0.6s">--}}
                        {{--<h3 class="f-title f_600 t_color f_size_18 mb_40">Workflow Solutions</h3>--}}
                        {{--<ul class="list-unstyled f_list">--}}
                            {{--<li><a href="#">Management</a></li>--}}
                            {{--<li><a href="#">Agile</a></li>--}}
                            {{--<li><a href="#">Task Management</a></li>--}}
                            {{--<li><a href="#">Reporting</a></li>--}}
                            {{--<li><a href="#">Work Tracking</a></li>--}}
                            {{--<li><a href="#">See All Uses</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-3 col-md-6">--}}
                    {{--<div class="f_widget about-widget pl_20 wow fadeInLeft" data-wow-delay="0.8s">--}}
                        {{--<h3 class="f-title f_600 t_color f_size_18 mb_40">Team Solutions</h3>--}}
                        {{--<ul class="list-unstyled f_list">--}}
                            {{--<li><a href="#">Engineering</a></li>--}}
                            {{--<li><a href="#">Designers</a></li>--}}
                            {{--<li><a href="#">Sales</a></li>--}}
                            {{--<li><a href="#">Developers</a></li>--}}
                            {{--<li><a href="#">Marketing</a></li>--}}
                            {{--<li><a href="#">See All Teams</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="footer_bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-5 col-sm-6">
                    <p class="mb-0 f_400">Copyright © 2019 Developed by <a href="https://www.xentech.io">Xentech</a></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                </div>
                <div class="col-lg-4 col-md-3 col-sm-6">
                    <div class="f_social_icon_two text-right">
                        <a href="https://www.facebook.com/YouthScholarship/"><i style="color:#00BB69;" class="ti-facebook"></i></a>
                        {{--<a href="#"><i class="ti-twitter-alt"></i></a>--}}
                        {{--<a href="#"><i class="ti-vimeo-alt"></i></a>--}}
                        {{--<a href="#"><i class="ti-pinterest"></i></a>--}}
                    </div>
                </div>
                {{--<div class="col-lg-4 col-md-4 col-sm-12">--}}
                {{--<ul class="list-unstyled f_menu text-right">--}}
                {{--<li><a href="#">Terms of Use</a></li>--}}
                {{--<li><a href="#">Privacy Policy</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}
            </div>
        </div>
</footer>