<!doctype html>
<html lang="en">

    <head>
        @include('front_end.partials.head')
    </head>

    <body>

        <div class="body_wrapper">
            @include('front_end.partials.header')
            <section class="breadcrumb_area"
                     style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('https://www.edutechlearning.com/myaccount/upload/workshop/final-training201803081121219653090.jpg'); background-position:  center center; background-repeat: no-repeat; background-size: cover;">
                <img class="breadcrumb_shap" src="" alt="">
                <div class="container">
                    <div class="breadcrumb_content text-center">
                        <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">{{$object->title}}</h1>
<!--
                        <p class="f_400 w_color f_size_16 l_height26">Why I say old chap that is spiffing off his nut
                            arse pear shaped plastered<br> Jeffrey bodge barney some dodgy.!!</p>
-->
                    </div>
                </div>
            </section>
            <section class="job_details_area sec_pad">
                <div class="container custom_container">
                    <div class="row flex-row-reverse">
                        <div class="col-lg-4 pl_30">
                            <div class="job_info">
                                <div class="info_head">
                                    <i class="ti-receipt"></i>
                                    <h6 class="f_p f_600 f_size_18 t_color3">Event Detail</h6>
                                </div>
                                <div class="info_item">
                                    <i class="ti-settings"></i>
                                    <h6>Category</h6>
                                    <p>{{\App\Http\Controllers\Dashboard\FunctionController::get_opportunity_type_name($object->type)}}</p>
                                </div>
                                <div class="info_item">
                                    <i class="ti-time"></i>
                                    <h6>Deadline</h6>
                                    <p>{{date('F j, Y', strtotime($object->deadline))}}</p>
                                </div>
                                {{--<div class="info_item">--}}
                                    {{--<i class="ti-timer"></i>--}}
                                    {{--<h6>Time Left</h6>--}}
                                    {{--<p>Planning</p>--}}
                                {{--</div>--}}
                                {{--<div class="info_item">--}}
                                    {{--<i class="ti-calendar"></i>--}}
                                    {{--<h6>Add to calender</h6>--}}
                                    {{--<p>Associate</p>--}}
                                {{--</div>--}}

                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="details_content">
                                <div class="sec_title mb_70">
                                    <img width="100%" src="{{$object->image}}" alt="">
                                    <h3 class="f_p f_size_22 f_500 t_color3 mt_30 mb_20">Job Requirements</h3>
                                    {!! nl2br($object->description) !!}
                                </div>
                                <div class="job_deatails_content">
                                    <h3 class="f_p f_size_22 f_500 t_color3 mb_20">Benefits</h3>
                                    {!! nl2br($object->benefits) !!}
                                </div>
                                <div class="job_deatails_content">
                                    <h3 class="f_p f_size_22 f_500 t_color3 mb_20">Eligibilities</h3>
                                    {!! nl2br($object->eligibilities) !!}


                                </div>
                                <div class="job_deatails_content">
                                    <h3 class="f_p f_size_22 f_500 t_color3 mb_20">Application Process:</h3>
                                    {!! nl2br($object->contact_info) !!}
                                </div>
                                <button  onclick="inc_appli('{{$object->id}}','opportunity','{{$object->application_link}}')" class="btn_three btn_hover">Apply Now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {{--<footer class="footer_area footer_area_four f_bg">--}}
                {{--<div class="footer_top">--}}
                    {{--<div class="container">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-4 pr_70">--}}
                                {{--<div class="f_widget company_widget">--}}
                                    {{--<a href="index.html" class="f-logo"><img src="img/logo - footer.png"--}}
                                                                             {{--srcset="img/logo2x.png 2x" alt=""></a>--}}
                                    {{--<div class="widget-wrap">--}}
                                        {{--<p class="f_400 f_p f_size_15 mb-0 l_height34"><span>Email:</span> <a--}}
                                                    {{--href="mailto:saasland@gmail.com"--}}
                                                    {{--class="f_400">saasland@gmail.com</a></p>--}}
                                        {{--<p class="f_400 f_p f_size_15 mb-0 l_height34"><span>Phone:</span> <a--}}
                                                    {{--href="tel:948256347968" class="f_400">+948 256 347 968</a></p>--}}
                                    {{--</div>--}}
                                    {{--<form action="#" class="f_subscribe mailchimp" method="post">--}}
                                        {{--<input type="text" name="EMAIL" class="form-control memail" placeholder="Email">--}}
                                        {{--<button class="btn btn-submit" type="submit"><i class="ti-arrow-right"></i>--}}
                                        {{--</button>--}}
                                        {{--<p class="mchimp-errmessage" style="display: none;"></p>--}}
                                        {{--<p class="mchimp-sucmessage" style="display: none;"></p>--}}
                                    {{--</form>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-8">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-lg-4">--}}
                                        {{--<div class="f_widget about-widget">--}}
                                            {{--<h3 class="f-title f_600 t_color f_size_18 mb_40">About Us</h3>--}}
                                            {{--<ul class="list-unstyled f_list">--}}
                                                {{--<li><a href="#">Company</a></li>--}}
                                                {{--<li><a href="#">Leadership</a></li>--}}
                                                {{--<li><a href="#">Diversity</a></li>--}}
                                                {{--<li><a href="#">Jobs</a></li>--}}
                                                {{--<li><a href="#">Press</a></li>--}}
                                                {{--<li><a href="#">Wavelength</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-lg-4">--}}
                                        {{--<div class="f_widget about-widget">--}}
                                            {{--<h3 class="f-title f_600 t_color f_size_18 mb_40">Workflow Solutions</h3>--}}
                                            {{--<ul class="list-unstyled f_list">--}}
                                                {{--<li><a href="#">Company</a></li>--}}
                                                {{--<li><a href="#">Leadership</a></li>--}}
                                                {{--<li><a href="#">Diversity</a></li>--}}
                                                {{--<li><a href="#">Jobs</a></li>--}}
                                                {{--<li><a href="#">Press</a></li>--}}
                                                {{--<li><a href="#">Wavelength</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-lg-4 pl-20">--}}
                                        {{--<div class="f_widget about-widget">--}}
                                            {{--<h3 class="f-title f_600 t_color f_size_18 mb_40">Team Solutions</h3>--}}
                                            {{--<ul class="list-unstyled f_list">--}}
                                                {{--<li><a href="#">Company</a></li>--}}
                                                {{--<li><a href="#">Leadership</a></li>--}}
                                                {{--<li><a href="#">Diversity</a></li>--}}
                                                {{--<li><a href="#">Jobs</a></li>--}}
                                                {{--<li><a href="#">Press</a></li>--}}
                                                {{--<li><a href="#">Wavelength</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="footer_bottom">--}}
                    {{--<div class="container">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-4 col-md-5 col-sm-6">--}}
                                {{--<p class="mb-0 f_400">Copyright © 2018 Desing by <a href="#">DroitThemes</a></p>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4 col-md-3 col-sm-6">--}}
                                {{--<div class="f_social_icon_two text-center">--}}
                                    {{--<a href="#"><i class="ti-facebook"></i></a>--}}
                                    {{--<a href="#"><i class="ti-twitter-alt"></i></a>--}}
                                    {{--<a href="#"><i class="ti-vimeo-alt"></i></a>--}}
                                    {{--<a href="#"><i class="ti-pinterest"></i></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4 col-md-4 col-sm-12">--}}
                                {{--<ul class="list-unstyled f_menu text-right">--}}
                                    {{--<li><a href="#">Terms of Use</a></li>--}}
                                    {{--<li><a href="#">Privacy Policy</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</footer>--}}
            @include('front_end.partials.footer_codes')
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        @include('front_end.partials.footer_links_for_index')
    </body>

</html>