<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
    protected $table = 'opportunity';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
