<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fund extends Model
{
    protected $table = 'funding_types';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
