<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Opp_type extends Model
{
    protected $table = 'opportunity_type';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
