<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
class EditController extends Controller
{
    public function delete_image(Request $request){
        if($request->ajax())
        {
            FunctionController::deleteImage($request->src);
            return Response('done');
        }
    }

    public function delete_object_call(Request $request){
        if($request->ajax()) {
            FunctionController::delete_object($request->id,$request->type);
            return Response($request);
        }

    }


    public function get_states(Request $request){
        if($request->ajax())
        {
            return Response(FunctionController::get_states($request->country_id));
        }
    }
    public function get_cities(Request $request){
        if($request->ajax())
        {

            return Response(FunctionController::get_cities($request->state_id));
        }
    }

    public function inc_application(Request $request){
        if($request->ajax())
        {
            FunctionController::inc_application($request->id,$request->type);
            return Response('done');

        }
    }


}
