<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Blog;
use App\Models\Degree;
use App\Models\Event;
use App\Models\Expo;
use App\Models\Fund;
use App\Models\Idea;
use App\Models\Job;
use App\Models\Opp_type;
use App\Models\Opportunity;
use App\Models\Organization;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Support\Facades\Auth;

class InsertController extends Controller
{
    public function insert_form(Request $request, $post_type)
    {



        if ($request->object_id) {
            $object = FunctionController::get_object($request->object_id, $post_type);
            $image_folder = $object->image_folder_id;
        } else {
            $image_folder = time();
        }
        $path = public_path() . '/images/' . $image_folder;
        File::makeDirectory($path, $mode = 0777, true, true);

        if($request->detail) {
            $detail=$request->detail;
        }
        else{
            $detail=default_description;
        }
        /*requirement start*/
        if ($request->requirement) {

            $requirement = $request->input('requirement');
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $requirement=str_replace('o:p','p',$requirement);
            $dom->loadHtml($requirement, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getElementsByTagName('img');

            foreach ($images as $k => $img) {
                if (substr($img->getattribute('src'), 0, 4) != 'http') {
                    $data = $img->getAttribute('src');
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name = "/images/" . $image_folder . '/requirement_' . time() . $k . '.png';
                    $path = public_path() . $image_name;
                    file_put_contents($path, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', prefix . ltrim($image_name, '/'));
                }
            }
            $requirement = $dom->saveHTML();
        }
        /*requirement end*/
        /*application process start*/
        if ($request->application_process) {
            $application_process=$request->application_process;
            }else{
            $application_process=default_description;
        }
        /*application process end*/
        /*benefits start*/
        if ($request->benefits) {
            $benefits = $request->input('benefits');
        }else{
            $benefits=default_description;
        }
        /*benefits end*/
        /*eligibilities start*/
        if ($request->eligibilities) {
            $eligibilities = $request->input('eligibilities');
        }else{
            $eligibilities=default_description;
        }
        /*eligibilities end*/
        if ($request->logo) {
            $logoName = 'logo_' . time() . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('/images'), $logoName);
            if ($request->object_id) {
                $object = FunctionController::get_object($request->object_id, $post_type);
                FunctionController::deleteImage($object->logo);
            }
        }

        if ($request->cover) {
            $coverName = 'cover_' . time() . '.' . $request->cover->getClientOriginalExtension();
            $request->cover->move(public_path('/images'), $coverName);
            if ($request->object_id) {
                $object = FunctionController::get_object($request->object_id, $post_type);
                FunctionController::deleteImage($object->image);
            }
        }
        if($post_type=='idea' ){
            if ($request->object_id) {
                $entity =Idea::findOrFail($request->object_id);
            } else {
                $entity = new Idea();
            }
            $entity->title=$request->title;
            $entity->uploader_id=auth()->user()->id;
            $entity->image_folder_id = $image_folder;
        }
        elseif($post_type=='blog' ){
            if ($request->object_id) {
                $entity =Blog::findOrFail($request->object_id);
            } else {
                $entity = new Blog();
            }
            $entity->title=$request->title;
            $entity->uploader_id=auth()->user()->id;
            $entity->image_folder_id = $image_folder;
        }

        elseif ($post_type == 'event' || $post_type=='expo') {

            if($post_type=='event') {
                if ($request->object_id) {
                    $entity = Event::findOrFail($request->object_id);
                } else {
                    $entity = new Event();
                }
            }else{
                if ($request->object_id) {
                    $entity = Expo::findOrFail($request->object_id);
                } else {
                    $entity = new Expo();
                }
            }
            $entity->title = $request->title;
            $entity->event_date = $request->date;
            $entity->address = $request->address;
            $entity->registration_link = $request->link;
            $entity->image_folder_id = $image_folder;
        } elseif ($post_type == 'organization') {
            if ($request->object_id) {
                $entity = Organization::findOrFail($request->object_id);
            } else {
                $entity = new Organization();
            }

            $entity->organization_name = $request->organization_name;
            if($request->logo) {
                $entity->logo = prefix . 'images/' . $logoName;
            }
            $entity->address = $request->address;
            $entity->contact_info = $request->contact_info;
            $entity->website_link = $request->link;
            $entity->image_folder_id = $image_folder;
        } elseif ($post_type == 'job') {

            if ($request->object_id) {
                $entity = Job::findOrFail($request->object_id);
            } else {
                $entity = new Job();
            }
            $entity->company_name = $request->company_name;
            $entity->founding_date = $request->founding_date;
            $entity->job_title = $request->job_title;
            $entity->requirement = $requirement;
            $entity->deadline = $request->deadline;
            $entity->image_folder_id = $image_folder;
        } else {
            if ($request->object_id) {
                $entity = Opportunity::findOrFail($request->object_id);
            } else {
                $entity = new Opportunity();
            }
            if(Auth::user()){
                $entity->approved=1;
            }else{
                $entity->approved=0;
            }
            $entity->title = $request->opportunity_title;
            $entity->type = $request->opportunity_type?: 'Undefined';
            $entity->application_process = $application_process;
            $entity->benefits = $benefits;
            $entity->eligibilities = $eligibilities;
            $entity->region=$request->region?: 'Undefined';
            $entity->funding_type=$request->funding_type?: 'Undefined';
            $entity->degree_type=$request->degree_type?: 'Undefined';
            $entity->address = $request->address?: 'Undefined';

            $entity->application_link = $request->link?: 'Undefined';
            $entity->website_link = $request->website_link?: 'Undefined';
            $entity->start_date = $request->start_date;
            $entity->end_date = $request->end_date;
            $entity->application_deadline = $request->deadline;

            $entity->country_id=$request->country_id?: 'Undefined';
            $entity->state_id=$request->state_id?: 'Undefined';
            $entity->city_id=$request->city_id?: 'Undefined';

            $entity->contact_info = $request->contact_info?: 'Undefined';
            $entity->image_folder_id = $image_folder;
        }

        $entity->description = $detail;

        if ($request->latitude) {
            $entity->latitude = 'N/A';
        }
        if ($request->longitude) {
            $entity->longitude = 'N/A';
        }
        if ($request->cover) {
            $entity->image = prefix . 'images/' . $coverName;
        }
        if(!$request->cover && !$request->object_id){
            $entity->image=default_cover;
        }
        $entity->save();
        if($request->object_id){
            return redirect('admin/list/'.$post_type)->with('message', 'Update Successful!');
        }
        return redirect()->back()->with('message', 'Upload Successful!');
    }

    public function insert_category(Request $request, $post_type){

        if($post_type=='explore_cover' || $post_type=='home_cover' || $post_type=='post_opp_cover'){
            $coverName = $post_type.'.jpg';
            $request->cover->move(public_path('dashboard/images/default_images'), $coverName);
        }
        else if($post_type=="funding_type") {
            if($request->object_id){
                $entity = Fund::findOrFail($request->object_id);
            }else{
                $entity = new Fund();
            }
            $entity->type=$request->type;
            $entity->save();
        }
        else if($post_type=="opportunity_type"){
            if($request->object_id){
                $entity = Opp_type::findOrFail($request->object_id);
            }else{
                $entity = new Opp_type();
            }
            $entity->type=$request->type;
            $entity->code=$request->code;
            if ($request->cover) {
                $coverName = 'cover_' . time() . '.' . $request->cover->getClientOriginalExtension();
                $request->cover->move(public_path('/images'), $coverName);
                if ($request->object_id) {
                    $object = FunctionController::get_object($request->object_id, $post_type);
                    FunctionController::deleteImage($object->image);
                }
                $entity->image = prefix . 'images/' . $coverName;
            }


            $entity->save();



        }elseif($post_type=='region'){
            if($request->object_id){
                $entity = Region::findOrFail($request->object_id);
            }else{
                $entity = new Region();
            }
            $entity->type=$request->type;
            $entity->save();
        }elseif($post_type=='degree_type'){
            if($request->object_id){
                $entity = Degree::findOrFail($request->object_id);
            }else{
                $entity = new Degree();
            }
            $entity->type=$request->type;
            $entity->save();
        }

        if($request->object_id){
            return redirect('admin/list/'.$post_type)->with('message', 'Update Successful!');
        }
        return redirect()->back()->with('message', 'Upload Successful!');
    }
}
