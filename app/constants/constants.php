<?php

define('prefix','http://'.$_SERVER['HTTP_HOST'].'/');
define('project_name','Youth Opportunity');
define('company_name','Turtle Venture');
define('color_codes',['#fa6fd1','#6fadfa','#f3af4e','#aa6ffa','#fa6666']);
define('default_description','No Information Provided Yet!');
define('default_cover','http://'.$_SERVER['HTTP_HOST'].'/dashboard/images/default_images/default_post_cover.jpg');
?>